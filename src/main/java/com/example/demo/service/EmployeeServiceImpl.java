package com.example.demo.service;


import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Employee;
import com.example.demo.repo.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeerepo;

	@Override
	public Employee saveEmployee(Employee emp) {
		// TODO Auto-generated method stub
		return employeerepo.save(emp);
	}

	@Override
	public Employee findById(Integer empId) {
		// TODO Auto-generated method stub
		Optional<Employee> emp= employeerepo.findById(empId);
		if(emp.isPresent()) {
			return emp.get();
		}else{
			return null;
		}

	}

//	@Override
//	public List<Employee> findAll() {
//		// TODO Auto-generated method stub
//		return employeerepo.findAll();
//	}
	
	@Override
	public List<Employee> findAll(int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		Pageable pageable= PageRequest.of(pageNumber, pageSize, Sort.by(Direction.ASC, "firstName"));
		return employeerepo.findAll(pageable).getContent();
	}

	@Override
	public String deleteEmployee(Integer empId) {
		// TODO Auto-generated method stub
		 employeerepo.deleteById(empId);;
		return "Success";
	}

	@Override
	public Employee updateEmployee(Employee emp, Integer empId) {
		// TODO Auto-generated method stub
		Employee dbEmp = findById(empId);
		dbEmp.setAge(emp.getAge());
		return employeerepo.save(dbEmp);

	}

	@Override
	public List<Employee> findByName(String firstName) {
		// TODO Auto-generated method stub
		//return employeerepo.findByFirstName(firstName);
		//return employeerepo.findByFirstNameContains(firstName);
		//return employeerepo.findByFirstNameContainsOrderByFirstNameAsc(firstName);
		//return employeerepo.findByFirstNameIs(firstName);
		//return employeerepo.findByFirstNameEquals(firstName);
		//return employeerepo.findByFirstNameLike(firstName);
		//return employeerepo.findByFirstNameNotLike(firstName);
		//return employeerepo.findByFirstNameStartingWith(firstName);
		return employeerepo.findByFirstNameContaining(firstName);
		//return employeerepo.findByFirstNameNot(firstName);
		//return employeerepo.findByFirstNameIsNull();
		//return employeerepo.findByFirstNameIsNotNull(); 
	}

	@Override
	public List<Employee> findByFNLN(String firstName, String lastName) {
		// TODO Auto-generated method stub
		//return employeerepo.findByFirstNameOrLastName(firstName, lastName);
		//return employeerepo.findByFirstNameOrLastNameContains(firstName, lastName);
		//return employeerepo.getUsersByName(firstName, lastName);
		return employeerepo.getUsersByNameSql(firstName, lastName);
				
	}

	@Override
	public List<Employee> getEmpByAge(int minAge, int maxAge) {
		// TODO Auto-generated method stub
		return employeerepo.findByAgeBetween(minAge, maxAge);
	}

	@Override
	public List<Employee> getEmpDetailsByAge(int age) {
		// TODO Auto-generated method stub
		//return employeerepo.findByAgeLessThan(age);
		//return employeerepo.findByAgeGreaterThan(age);
		//return employeerepo.findByAgeGreaterThanEqual(age);
		return employeerepo.findByAgeAfter(age);
	}

	@Override
	public List<Employee> findByAgeIn(Collection<Integer> ages) {
		// TODO Auto-generated method stub
		return employeerepo.findByAgeIn(ages);
		//return employeerepo.findByAgeNotIn(ages);
	}
	

}
