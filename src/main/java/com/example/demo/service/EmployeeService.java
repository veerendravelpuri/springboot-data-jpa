package com.example.demo.service;

import java.util.*;

import com.example.demo.entity.Employee;

public interface EmployeeService {

	Employee saveEmployee(Employee emp);

	Employee findById(Integer empId);
	
	List<Employee> findAll(int pageNumber, int pageSize);
	
	String deleteEmployee(Integer empId);

	Employee updateEmployee(Employee emp, Integer empId);
	
	List<Employee> findByName(String firstName);

	List<Employee> findByFNLN(String firstName, String lastName);

	List<Employee> getEmpByAge(int minAge, int MaxAge);
	
	List<Employee> getEmpDetailsByAge(int age);
	
	List<Employee> findByAgeIn(Collection<Integer> ages);

}
