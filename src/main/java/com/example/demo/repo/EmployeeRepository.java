package com.example.demo.repo;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer> {

	 List<Employee> findByFirstName(String firstName);
	 
	 List<Employee> findByFirstNameContains(String firstName);
	 
	 List<Employee> findByFirstNameContainsOrderByFirstNameAsc(String firstName);
	 
	 List <Employee> findByFirstNameOrLastName(String firstName, String lastName);
	 
	 List<Employee> findByFirstNameAndLastName(String firstName, String lastName);
	 
	 List<Employee> findByFirstNameOrLastNameContains(String firstName,String lastName);
	 
	 @Query("from Employee where firstName = :firstName and lastName = :lastName")
	 List<Employee> getUsersByName(String firstName, String lastName);
	 
	 @Query(value="select e.* from employee e where e.first_name = :firstName and e.last_name = :lastName",nativeQuery = true)
	 List<Employee> getUsersByNameSql(String firstName, String lastName);
	 
	 List<Employee> findByFirstNameIs(String firstName);
	 
	 List<Employee> findByFirstNameEquals(String firstName);
	 
	 List<Employee> findByAgeBetween(int minAge, int maxAge);
	 
	 List<Employee> findByAgeLessThan(int age);
	 
	 List<Employee> findByAgeGreaterThan(int age);
	 
	 List<Employee> findByAgeGreaterThanEqual(int age);
	 
	 List<Employee> findByAgeAfter(int age);
	 
	 List<Employee> findByFirstNameIsNull();
	 
	 List<Employee> findByFirstNameIsNotNull();
	 
	 List<Employee> findByFirstNameLike(String firstName);
	 
	 List<Employee> findByFirstNameNotLike(String firstName);
	 
	 List<Employee> findByFirstNameStartingWith(String firstName);
	 
	 List<Employee> findByFirstNameEndingWith(String firstName);
	 
	 List<Employee> findByFirstNameContaining(String firstName);
	 
	 List<Employee> findByAgeIn(Collection<Integer> ages);
	 
	 List<Employee> findByAgeNotIn(Collection<Integer> ages);
	 
	 List<Employee> findByFirstNameNot(String firstName);
	 
	 List<Employee> findByFirstNameIgnoreCase(String firstName);
}
