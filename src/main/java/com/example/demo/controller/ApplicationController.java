package com.example.demo.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Employee;
import com.example.demo.service.EmployeeService;

@RestController
@RequestMapping("/employees")
public class ApplicationController {

	@Autowired
	EmployeeService empService;

	
	@PostMapping("/add")
	public Employee saveDetails(@RequestBody Employee employee) {
		return empService.saveEmployee(employee);
	}
	
	@GetMapping("/{empId}")
	public Employee findById(@PathVariable Integer empId) {
		System.out.println(empId);
		return empService.findById(empId);
	}
	
	@GetMapping
	public List<Employee> findAll(@RequestParam int pageNumber,@RequestParam int pageSize){	
		return empService.findAll(pageNumber,pageSize);
	}
	
	@DeleteMapping("/{empId}")
	public String deleteEmployee(@PathVariable Integer empId) {
		return empService.deleteEmployee(empId);		
	}
	
	@PutMapping("/update/{empId}")
	public Employee updateEmployee(@RequestBody Employee emp,@PathVariable Integer empId) {
		return empService.updateEmployee(emp,empId);
	}
	
	@GetMapping("/getByName")
	public List<Employee> findByName(@RequestParam String firstName) {
		return empService.findByName(firstName) ;
	}
	
	@GetMapping("/getByFNLN")
	public List<Employee> getEmpsByFNLN(@RequestParam String firstName, @RequestParam String lastName){
		return empService.findByFNLN(firstName,lastName);
	}
	
	@GetMapping("/GetByAgeBetween")
	public List<Employee> getEmpByAge(@RequestParam int minAge, @RequestParam int maxAge){
		return empService.getEmpByAge(minAge, maxAge);
	}
	
	@GetMapping("/getByAge")
	public List<Employee> getEmpDetailsByAge(@RequestParam int age){
		return empService.getEmpDetailsByAge(age);
	}
	
	@GetMapping("/getByAgesIn")
	public 	List<Employee> findByAgeIn(@RequestParam Collection<Integer> ages){
		return empService.findByAgeIn(ages);
		
	}

}
